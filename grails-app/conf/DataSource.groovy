dataSource {
    pooled = true
    //driverClassName = "com.mysql.jdbc.Driver"
	url = "jdbc:oracle:thin:@opendb:1521:XE"
	driverClassName = "oracle.jdbc.OracleDriver"
    username = "test"
    //password = "password"
	password = "abcd1234"
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
			//url = "jdbc:mysql://192.168.216.132:3306/test?autoreconnect=true"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
			url = "jdbc:mysql://192.168.216.132:3306/test?autoreconnect=true"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
			url = "jdbc:mysql://192.168.216.132:3306/test?autoreconnect=true"
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
